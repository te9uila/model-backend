## 后端开发模板

### maven 列表 (已使用) 

1. springboot 系列
2. Lombok 工具包
3. mybatis-plus
4. mysql 数据库驱动
5. hutool 工具包

### 修改文件

1. `src/main/resources/application.yml`

   ```yml
   server:
       port: 10086 # 端口
       servlet:
           context-path: /model # 默认路径
   
   spring:
       jackson:
           date-format: yyyy-MM-dd HH:mm:ss
           time-zone: GMT+8
   
       datasource:
           driver-class-name: com.mysql.cj.jdbc.Driver
           url: jdbc:mysql://localhost:3306/test  # 数据库url
           username: root # 数据库账号
           password: 123456 # 密码
   
   mybatis-plus:
       global-config:
           db-config:
               id-type: auto
       mapper-locations: classpath:mapper/*.xml
       configuration:
           map-underscore-to-camel-case: true
           log-impl: org.apache.ibatis.logging.stdout.StdOutImpl
   ```

2. 测试数据库

   ```sql
   create table if not exists test
   (
       id       varchar(36)  not null primary key,
       username varchar(255) not null,
       password varchar(255) not null
   );
   ```

   

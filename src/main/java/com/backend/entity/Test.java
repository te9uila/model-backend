package com.backend.entity;

import lombok.Data;

/**
 * @author te9uila
 * @since 2024/2/2
 */
@Data
public class Test {

    private String id;
    private String username;
    private String password;
}

package com.backend.service;

import com.backend.entity.Test;
import com.backend.response.Result;

/**
 * @author te9uila
 * @since 2024/2/2
 */
public interface TestService {

    Result<Object> add(Test test);

}

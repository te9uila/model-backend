package com.backend.service.impl;

import com.backend.entity.Test;
import com.backend.mapper.TestMapper;
import com.backend.response.Result;
import com.backend.response.ResultEnum;
import com.backend.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author te9uila
 * @since 2024/2/2
 */
@Service
public class TextServiceImpl implements TestService {

    private final TestMapper testMapper;

    @Autowired
    public TextServiceImpl(TestMapper testMapper) {
        this.testMapper = testMapper;
    }

    @Override
    public Result<Object> add(Test test) {
        testMapper.insert(test);
        return new Result<>(ResultEnum.INSERT_SUCCESS);
    }
}

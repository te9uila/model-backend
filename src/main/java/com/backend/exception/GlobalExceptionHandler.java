package com.backend.exception;

import com.backend.response.Result;
import com.backend.response.ResultEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author te9uila
 * @since 2024/2/2
 */

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public Result<Object> handleException(Exception exception){
        log.error("全局异常捕获,{}", exception.getMessage());
        return new Result<>(ResultEnum.SERVER_ERROR);
    }
}

package com.backend.controller;

import com.backend.entity.Test;
import com.backend.response.Result;
import com.backend.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author te9uila
 * @since 2024/2/2
 */

@RestController
@RequestMapping("/test")
public class TestController {

    private final TestService testService;

    @Autowired
    public TestController(TestService testService) {
        this.testService = testService;
    }

    @PostMapping("/add")
    public Result<Object> add(@RequestBody Test test){
        return testService.add(test);
    }
}

package com.backend.response;

import lombok.Getter;

/**
 * @author te9uila
 * @since 2024/2/2
 */
@Getter
public enum ResultEnum {
    SELECT_SUCCESS(200,"查询成功"),
    UPDATE_SUCCESS(200,"修改成功"),
    INSERT_SUCCESS(200,"新增成功"),
    DELETE_SUCCESS(200,"删除成功"),

    SELECT_FAIL(500,"查询失败"),
    UPDATE_FAIL(500,"修改失败"),
    INSERT_FAIL(500,"新增失败"),
    DELETE_FAIL(500,"删除失败"),

    SERVER_ERROR(500,"服务异常")
    ;

    private final Integer code;

    private final String message;

    ResultEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}

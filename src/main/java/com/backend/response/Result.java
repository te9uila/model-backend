package com.backend.response;

import lombok.Data;

/**
 * @author te9uila
 * @since 2024/2/2
 */
@Data
public class Result<T> {

    // 状态码
    // 200成功, 500失败, 401未登录, 403没有权限
    private Integer code;

    // 新增成功
    private String message;

    private T data;

    public Result(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Result(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Result(ResultEnum resultEnum) {
        this.code = resultEnum.getCode();
        this.message = resultEnum.getMessage();
    }
}

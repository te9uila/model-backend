package com.backend.mapper;

import com.backend.entity.Test;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author te9uila
 * @since 2024/2/2
 */
public interface TestMapper extends BaseMapper<Test> {
}
